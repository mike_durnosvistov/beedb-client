from pyvcloud import vcloudair

from beedb_client.v1 import baseclient


class vCloudAirBeeDBClient(baseclient.BeeDBClient):

    def __init__(self, beedb_endpoint, user, password, host, service_type,
                 service_version, instance, service, org_name,
                 insecure=False, enable_logging=False):
        """
        vCloud Air client for BeeDB
        :param basestring beedb_endpoint: BeeDB API endpoint
        :param basestring user: vCloud user
        :param basestring host: vCloud host
        :param basestring password: vCloud user passowrd
        :param str service_type: vCloud service type
                          (ondemand or subscription)
        :param service_version: vCloud service type version
        :param instance: vCloud use account instance
        :param service: vCloud organization service
        :param org_name: vCloud organization name
        :param insecure: secure/insecure vCloud SSL/TLS connection
        :param enable_logging: enable/disable logging for HTTP requests
        :return:
        """
        if not any([beedb_endpoint, user, password, host, service_type,
                    service_version, host,
                    instance or (org_name and service)]):
            raise Exception("Invalid credentials. "
                            "It is necessary to specify: "
                            "API endpoint, username, password, "
                            "host, service version, service type and "
                            "instance or organization and "
                            "organization service "
                            "to get authorization")

        self.authorized_vca = self._authenticate(
            user, password, host, service_type,
            service_version, instance, service, org_name)

        super(vCloudAirBeeDBClient, self).__init__(
            beedb_endpoint, auth_headers=self.headers,
            verify=not insecure, enable_log=enable_logging)

    def _get_service_type(self, service_type):
        unknown = vcloudair.VCA.VCA_SERVICE_TYPE_UNKNOWN
        _types = {
            'standalone': vcloudair.VCA.VCA_SERVICE_TYPE_STANDALONE,
            'ondemand': vcloudair.VCA.VCA_SERVICE_TYPE_VCA,
            'subscription': vcloudair.VCA.VCA_SERVICE_TYPE_VCHS,
        }
        return _types.get(service_type, unknown)

    def _authenticate(self, user, password, host,
                      service_type,
                      service_version, instance,
                      service, org_name):
        """
        Logs user into vCloud service
        :param basestring user: vCloud user
        :param basestring host: vCloud host
        :param basestring password: vCloud user passowrd
        :param str service_type: vCloud service type
                          (ondemand or subscription)
        :param service_version: vCloud service type version
        :param instance: vCloud use account instance
        :param service: vCloud organization service
        :param org_name: vCloud organization name
        :return:
        """
        try:
            vca = vcloudair.VCA(
                host, user,
                service_type=self._get_service_type(service_type),
                version=service_version)
            result = vca.login(password=password)
            if result:
                if self._is_ondemand(service_type):
                    if not instance:
                        if not vca.instances:
                            return None
                        instance = vca.instances[0]['id']
                    result = vca.login_to_instance(instance, password)
                elif self._is_subscription(service_type):
                    if not service:
                        if org_name:
                            service = org_name
                        else:
                            services = vca.services.get_Service()
                            if not services:
                                return None
                            service = services[0].serviceId
                    if not org_name:
                        org_name = vca.get_vdc_references(service)[0].name
                    result = vca.login_to_org(service, org_name)
                if (result and
                        hasattr(vca, 'vcloud_session') and
                        hasattr(vca.vcloud_session, 'org_url')):
                            return vca
            else:
                print(vca.response._content)
                raise Exception("Unauthorized. Invalid credentials.")
        except Exception as e:
            raise e

    def _set_host(self, host, service_type):
        if host:
            return self._add_prefix(host)
        if self._is_ondemand(service_type):
            return "https://vca.vmware.com"
        else:
            return "https://vchs.vmware.com"

    def _add_prefix(self, host):
        if not (host.startswith('https://') or host.startswith('http://')):
            host = 'https://' + host
        return host

    def _is_ondemand(self, service_type):
        return self._compare(service_type, 'ondemand')

    def _is_subscription(self, service_type):
        return self._compare(service_type, 'subscription')

    def _compare(self, service_type, string):
        return service_type.lower().strip() == string

    @property
    def tenant(self):
        """
        Gets tenant attribute. In case of vCloud unique
        identifier is organization ID
        :return:
        """
        return self.authorized_vca.vcloud_session.org_url.split('/')[-1]

    def _get_auth_headers(self):
        return {
            "x-vcloud-authorization":
                self.authorized_vca.vcloud_session.token,
            "x-vcloud-org-url":
                self.authorized_vca.vcloud_session.org_url,
            "x-vcloud-version": self.authorized_vca.version,
            'x-beedb-auth-platform': 'vcloudair',
        }
