import os

from oslo_config import cfg

from beedb_client.common import cli_application
from beedb_client.common import print_utils
from beedb_client.v1.aws import client


class Databases(object):

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--aws-sts-endpoint-url', dest='sts_endpoint_url',
                          help="AWS STS endpoint url",
                          default=os.environ.get('AWS_STS_ENDPOINT_URL'))
    @cli_application.args('--access-key-id', dest='access_key_id',
                          help="AWS user access_key_id",
                          default=os.environ.get('ACCESS_KEY_ID'))
    @cli_application.args('--secret-access-key', dest='secret_access_key',
                          help="AWS user secret_access_key",
                          default=os.environ.get('SECRET_ACCESS_KEY'))
    @cli_application.args('--region', dest='region',
                          help="AWS region",
                          default=os.environ.get('AWS_REGION'))
    def list(self, beedb_endpoint=None, sts_endpoint_url=None, region=None,
             access_key_id=None, secret_access_key=None):
        """
        Lists databases
        :param beedb_endpoint: BeeDB API endpoint
        :param access_key_id: AWS user access_key_id
        :param sts_endpoint_url: AWS STS endpoint url
        :param secret_access_key: AWS user secret_access_key
        :param region: AWS service region
        :return:
        """

        bee = client.AWSBeeDBClient(beedb_endpoint, sts_endpoint_url,
                                    access_key_id, secret_access_key, region)
        print_utils.print_list(bee.databases.list(),
                               ['name', 'status',
                                'tenant', 'database_type',
                                'url', 'created_at',
                                'updated_at'])

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--aws-sts-endpoint-url', dest='sts_endpoint_url',
                          help="AWS STS endpoint url",
                          default=os.environ.get('AWS_STS_ENDPOINT_URL'))
    @cli_application.args('--database', dest='database',
                          help='Database identifier')
    @cli_application.args('--access-key-id', dest='access_key_id',
                          help="AWS user access_key_id",
                          default=os.environ.get('ACCESS_KEY_ID'))
    @cli_application.args('--secret-access-key', dest='secret_access_key',
                          help="AWS user secret_access_key",
                          default=os.environ.get('SECRET_ACCESS_KEY'))
    @cli_application.args('--region', dest='region',
                          help="AWS region",
                          default=os.environ.get('AWS_REGION'))
    def get(self, beedb_endpoint=None, sts_endpoint_url=None,
            access_key_id=None,
            secret_access_key=None, region=None, database=None):
        """
        Shows database description
        :param beedb_endpoint: BeeDB API endpoint
        :param access_key_id: AWS user access_key_id
        :param secret_access_key: AWS user secret_access_key
        :param database: Given database name
        :param sts_endpoint_url: AWS STS endpoint url
        :param region: AWS service region
        :return:
        """
        bee = client.AWSBeeDBClient(beedb_endpoint, sts_endpoint_url,
                                    access_key_id, secret_access_key, region)
        print_utils.print_dict(bee.databases.get(database).json_object)

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--aws-sts-endpoint-url', dest='sts_endpoint_url',
                          help="AWS STS endpoint url",
                          default=os.environ.get('AWS_STS_ENDPOINT_URL'))
    @cli_application.args('--database', dest='database',
                          help='Database identifier')
    @cli_application.args('--description', dest='description',
                          help="Database description.")
    @cli_application.args('--access-key-id', dest='access_key_id',
                          help="AWS user access_key_id",
                          default=os.environ.get('ACCESS_KEY_ID'))
    @cli_application.args('--secret-access-key', dest='secret_access_key',
                          help="AWS user secret_access_key",
                          default=os.environ.get('SECRET_ACCESS_KEY'))
    @cli_application.args('--region', dest='region',
                          help="AWS region",
                          default=os.environ.get('AWS_REGION'))
    @cli_application.args('--role-name', dest='role',
                          help="Role for database assigning.")
    def create(self, beedb_endpoint=None, region=None, sts_endpoint_url=None,
               access_key_id=None, secret_access_key=None,
               database=None, description=None, role=None):
        """
        Creates a database by given name and description
        :param beedb_endpoint: BeeDB API endpoint
        :param access_key_id: AWS user access_key_id
        :param secret_access_key: AWS user secret_access_key
        :param database: Given database name
        :param description: Given database description
        :param role: role to assign to database
        :param sts_endpoint_url: AWS STS endpoint url
        :param region: AWS service region
        :return:
        """
        bee = client.AWSBeeDBClient(beedb_endpoint, sts_endpoint_url,
                                    access_key_id, secret_access_key, region)
        print_utils.print_dict(bee.databases.create(
            database,
            role,
            description=description).json_object)

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--aws-sts-endpoint-url', dest='sts_endpoint_url',
                          help="AWS STS endpoint url",
                          default=os.environ.get('AWS_STS_ENDPOINT_URL'))
    @cli_application.args('--database', dest='database',
                          help='Database identifier')
    @cli_application.args('--access-key-id', dest='access_key_id',
                          help="AWS user access_key_id",
                          default=os.environ.get('ACCESS_KEY_ID'))
    @cli_application.args('--secret-access-key', dest='secret_access_key',
                          help="AWS user secret_access_key",
                          default=os.environ.get('SECRET_ACCESS_KEY'))
    @cli_application.args('--region', dest='region',
                          help="AWS region",
                          default=os.environ.get('AWS_REGION'))
    def delete(self, beedb_endpoint=None, region=None, sts_endpoint_url=None,
               access_key_id=None, secret_access_key=None,
               database=None):
        """
        Deletes database
        :param beedb_endpoint: BeeDB API endpoint
        :param access_key_id: AWS user access_key_id
        :param secret_access_key: AWS user secret_access_key
        :param sts_endpoint_url: AWS STS endpoint url
        :param region: AWS service region
        :return:
        """
        bee = client.AWSBeeDBClient(beedb_endpoint, sts_endpoint_url,
                                    access_key_id, secret_access_key, region)
        bee.databases.delete(database)
        print("OK. Done.")


class Roles(object):

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--aws-sts-endpoint-url', dest='sts_endpoint_url',
                          help="AWS STS endpoint url",
                          default=os.environ.get('AWS_STS_ENDPOINT_URL'))
    @cli_application.args('--role-name', dest='role',
                          help='Role name')
    @cli_application.args('--description', dest='description',
                          help="Database description.")
    @cli_application.args('--access-key-id', dest='access_key_id',
                          help="AWS user access_key_id",
                          default=os.environ.get('ACCESS_KEY_ID'))
    @cli_application.args('--secret-access-key', dest='secret_access_key',
                          help="AWS user secret_access_key",
                          default=os.environ.get('SECRET_ACCESS_KEY'))
    @cli_application.args('--region', dest='region',
                          help="AWS region",
                          default=os.environ.get('AWS_REGION'))
    @cli_application.args('--role-password', dest='role_password',
                          help="Role for database assigning.")
    @cli_application.args('--database-type', dest='database_type',
                          help="Role for database assigning.")
    def create(self, beedb_endpoint=None, region=None, sts_endpoint_url=None,
               access_key_id=None, secret_access_key=None,
               role=None, description=None,
               role_password=None, database_type=None):
        """
        Creates a database by given name and description
        :param beedb_endpoint: BeeDB API endpoint
        :param access_key_id: AWS user access_key_id
        :param secret_access_key: AWS user secret_access_key
        :param role_password: Given role password
        :param description: Given database description
        :param sts_endpoint_url: AWS STS endpoint url
        :param region: AWS service region
        :return:
        """
        bee = client.AWSBeeDBClient(beedb_endpoint, sts_endpoint_url,
                                    access_key_id, secret_access_key, region)
        print_utils.print_dict(bee.roles.create(
            role,
            role_password,
            database_type,
            description=description).json_object)

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--aws-sts-endpoint-url', dest='sts_endpoint_url',
                          help="AWS STS endpoint url",
                          default=os.environ.get('AWS_STS_ENDPOINT_URL'))
    @cli_application.args('--access-key-id', dest='access_key_id',
                          help="AWS user access_key_id",
                          default=os.environ.get('ACCESS_KEY_ID'))
    @cli_application.args('--secret-access-key', dest='secret_access_key',
                          help="AWS user secret_access_key",
                          default=os.environ.get('SECRET_ACCESS_KEY'))
    @cli_application.args('--region', dest='region',
                          help="AWS region",
                          default=os.environ.get('AWS_REGION'))
    def list(self, beedb_endpoint=None, region=None, sts_endpoint_url=None,
             access_key_id=None, secret_access_key=None):
        """
        Lists databases
        :param beedb_endpoint: BeeDB API endpoint
        :param access_key_id: AWS user access_key_id
        :param secret_access_key: AWS user secret_access_key
        :param sts_endpoint_url: AWS STS endpoint url
        :param region: AWS service region
        :return:
        """

        bee = client.AWSBeeDBClient(beedb_endpoint, sts_endpoint_url,
                                    access_key_id, secret_access_key, region)
        print_utils.print_list(bee.roles.list(),
                               ['name', 'status',
                                'tenant', 'database_type',
                                'password', 'created_at',
                                'updated_at'])

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--aws-sts-endpoint-url', dest='sts_endpoint_url',
                          help="AWS STS endpoint url",
                          default=os.environ.get('AWS_STS_ENDPOINT_URL'))
    @cli_application.args('--role-name', dest='role',
                          help='Role name')
    @cli_application.args('--access-key-id', dest='access_key_id',
                          help="AWS user access_key_id",
                          default=os.environ.get('ACCESS_KEY_ID'))
    @cli_application.args('--secret-access-key', dest='secret_access_key',
                          help="AWS user secret_access_key",
                          default=os.environ.get('SECRET_ACCESS_KEY'))
    @cli_application.args('--region', dest='region',
                          help="AWS region",
                          default=os.environ.get('AWS_REGION'))
    def get(self, beedb_endpoint=None, region=None, sts_endpoint_url=None,
            access_key_id=None, secret_access_key=None,
            role=None):
        """
        Shows database description
        :param beedb_endpoint: BeeDB API endpoint
        :param access_key_id: AWS user access_key_id
        :param secret_access_key: AWS user secret_access_key
        :param role: role to assign to database
        :param sts_endpoint_url: AWS STS endpoint url
        :param region: AWS service region
        :return:
        """
        bee = client.AWSBeeDBClient(beedb_endpoint, sts_endpoint_url,
                                    access_key_id, secret_access_key, region)
        print_utils.print_dict(bee.roles.get(role).json_object)

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--aws-sts-endpoint-url', dest='sts_endpoint_url',
                          help="AWS STS endpoint url",
                          default=os.environ.get('AWS_STS_ENDPOINT_URL'))
    @cli_application.args('--role-name', dest='role',
                          help='Role name')
    @cli_application.args('--access-key-id', dest='access_key_id',
                          help="AWS user access_key_id",
                          default=os.environ.get('ACCESS_KEY_ID'))
    @cli_application.args('--secret-access-key', dest='secret_access_key',
                          help="AWS user secret_access_key",
                          default=os.environ.get('SECRET_ACCESS_KEY'))
    @cli_application.args('--region', dest='region',
                          help="AWS region",
                          default=os.environ.get('AWS_REGION'))
    def delete(self, beedb_endpoint=None, region=None, sts_endpoint_url=None,
               access_key_id=None, secret_access_key=None,
               role=None):
        """
        Deletes database
        :param beedb_endpoint: BeeDB API endpoint
        :param access_key_id: AWS user access_key_id
        :param secret_access_key: AWS user secret_access_key
        :param role: role to assign to database
        :param sts_endpoint_url: AWS STS endpoint url
        :param region: AWS service region
        :return:
        """
        bee = client.AWSBeeDBClient(beedb_endpoint, sts_endpoint_url,
                                    access_key_id, secret_access_key, region)
        bee.roles.delete(role)
        print("OK. Done.")


class DatabaseTypes(object):

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--aws-sts-endpoint-url', dest='sts_endpoint_url',
                          help="AWS STS endpoint url",
                          default=os.environ.get('AWS_STS_ENDPOINT_URL'))
    @cli_application.args('--access-key-id', dest='access_key_id',
                          help="AWS user access_key_id",
                          default=os.environ.get('ACCESS_KEY_ID'))
    @cli_application.args('--secret-access-key', dest='secret_access_key',
                          help="AWS user secret_access_key",
                          default=os.environ.get('SECRET_ACCESS_KEY'))
    @cli_application.args('--region', dest='region',
                          help="AWS region",
                          default=os.environ.get('AWS_REGION'))
    def list(self, beedb_endpoint=None, region=None, sts_endpoint_url=None,
             access_key_id=None, secret_access_key=None):
        bee = client.AWSBeeDBClient(beedb_endpoint, sts_endpoint_url,
                                    access_key_id, secret_access_key, region)
        print_utils.print_list(bee.database_types.list(),
                               ['name', 'description',
                                'created_at', 'updated_at'])

CATS = {
    'databases': Databases,
    'roles': Roles,
    'database-types': DatabaseTypes,
}

category_opt = cfg.SubCommandOpt(
    'category',
    title='Command categories',
    help='Available categories',
    handler=cli_application.add_command_parsers(CATS)
)


def main():
    """Parse options and call the appropriate class/method."""
    cli_application.main(cfg.CONF, category_opt)

if __name__ == "__main__":
    main()
