import botocore.session

from beedb_client.v1 import baseclient


class AWSBeeDBClient(baseclient.BeeDBClient):

    def __init__(self, beedb_endpoint, endpoint_url,
                 access_key_id, secret_access_key, region=None,
                 enable_logging=False, insecure=False):
        """
        :param basestring beedb_endpoint: BeeDB API endpoint.
                                        Example: https://beedb.com:8888/v1
        :param endpoint_url: The complete URL for connecting to STS
                             to use for the constructed client.
        :param basestring access_key_id: Access Key Id for authentication.
        :param basestring secret_access_key: Secret Access Key for
                                             authentication.
        :param enable_logging: enable/disable logging for HTTP requests
        """

        if not any([beedb_endpoint, endpoint_url,
                    access_key_id, secret_access_key]):
            raise Exception("Invalid credentials. "
                            "It is necessary to specify: "
                            "API endpoint, access_key_id, secret_access_key, "
                            "auth URL and project ID or project "
                            "name to get authorization")
        self._client_id = self._get_user_id(access_key_id, secret_access_key,
                                            region)
        self.aws_endpoint_url = endpoint_url
        self.sts_client = self._authenticate('sts', access_key_id,
                                             secret_access_key, region=region)
        self.session_token = self.sts_client.get_session_token(
            DurationSeconds=900)['Credentials']['SessionToken']

        super(AWSBeeDBClient, self).__init__(
            beedb_endpoint, verify=not insecure, enable_log=enable_logging)

    @property
    def tenant(self):
        """
        Gets client_id unique attribute for given Indentity tenant
        :return client_id
        :rtype: basestring
        """
        return self._client_id

    def _get_user_id(self, access_key_id, secret_access_key, region):
        client_iam = self._authenticate('iam', access_key_id,
                                        secret_access_key, region=region)
        return client_iam.get_user()['User']['Arn'].split(':')[4]

    def _authenticate(self, aws_service, access_key_id, secret_access_key,
                      region=None):
        """
        :return: Return AWS Account ID
        """
        session = botocore.session.get_session()
        client = session.create_client(aws_service, region_name=region,
                                       aws_access_key_id=access_key_id,
                                       aws_secret_access_key=secret_access_key)
        return client

    def _get_auth_headers(self):
        """
        Builds headers for authorization workflow
        :return: headers: Request headers that are necessary
                          to authorize user at BeeDB API service
        :rtype: dict
        """
        return {
            'x-aws-session-token': self.session_token,
            'x-aws-region-name': self.sts_client.meta.region_name,
            'x-aws-endpoint-url': self.aws_endpoint_url,
            'x-beedb-auth-platform': 'aws',
        }
