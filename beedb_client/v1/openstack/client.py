from beedb_client.v1 import baseclient


class OpenStackBeeDBClient(baseclient.BeeDBClient):

    def __init__(self, beedb_endpoint,
                 username, password, project_name,
                 auth_url, auth_token, project_id,
                 auth_region, enable_logging=False,
                 insecure=False):
        """
        :param basestring beedb_endpoint: BeeDB API endpoint.
                                        Example: https://beedb.com:8888/v1
        :param basestring username: Username for authentication. (optional)
        :param basestring password: Password for authentication. (optional)
        :param basestring auth_url: Identity service endpoint
                                    for authorization.
        :param basestring auth_region: Name of a region to select
                                       when choosing an
                                       endpoint from the service catalog.
        :param basestring auth_token: Token for authentication. (optional)
        :param string project_name: Project name.
        :param string project_id: Project id. (optional)
        """

        self.api_url = beedb_endpoint
        self.username = username
        self.password = password
        self.project_name = project_name
        self.auth_url = auth_url
        self.auth_token = auth_token
        self.project_id = project_id
        self.auth_region = auth_region

        if not any([self.api_url, self.username,
                    self.password, self.auth_url,
                    self.auth_region,
                    self.project_id or
                    self.project_name]) or not any(
            [self.api_url, self.auth_token,
             self.auth_region, self.auth_url]):
            raise Exception("Invalid credentials. "
                            "It is necessary to specify: "
                            "API endpoint, username, password, "
                            "auth URL and project ID or project "
                            "name to get authorization")

        self.authorized_keystoneclient = self._authenticate()

        super(OpenStackBeeDBClient, self).__init__(
            self.api_url, auth_headers=self.headers,
            verify=not insecure, enable_log=enable_logging)

    @property
    def tenant(self):
        """
        Gets tenant unique attribute for given Indentity tenant
        :return tenant
        :rtype: basestring
        """
        return self.authorized_keystoneclient.tenant_id

    def _get_auth_headers(self):
        """
        Builds headers for authorization workflow
        :return: headers: Request headers that are necessary
                          to authorize user at BeeDB API service
        :rtype: dict
        """
        return {
            'x-openstack-auth-token':
                self.authorized_keystoneclient.auth_token,
            'x-openstack-auth-url': self.auth_url,
            'x-openstack-region-name': self.auth_region,
            'x-beedb-auth-platform': 'openstack',
        }

    def _authenticate(self):
        """
        Builds Keystone client and tries to
        authorize within given credentials
        :return: Keystone authorized client
        """

        if 'v2' in self.auth_url:
            from keystoneclient.v2_0 import client
        else:
            from keystoneclient.v3 import client

        if self.auth_token:
            credentials = {
                'token': self.auth_token,
                'auth_url': self.auth_url,
                'region_name': self.auth_region,
            }
        else:
            credentials = {
                'username': self.username,
                'password': self.password,
                'project_name': self.project_name,
                'project_id': self.project_id,
                'region_name': self.auth_region,
                'auth_url': self.auth_url
            }

        keystone = client.Client(**credentials)
        keystone.authenticate()
        return keystone
