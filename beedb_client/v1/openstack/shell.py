import os

from oslo_config import cfg

from beedb_client.common import cli_application
from beedb_client.common import print_utils
from beedb_client.v1.openstack import client


class Databases(object):

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--username', dest='username',
                          help="OpenStack username",
                          default=os.environ.get('OS_USERNAME'))
    @cli_application.args('--password', dest='password',
                          help="OpenStack user password",
                          default=os.environ.get('OS_PASSWORD'))
    @cli_application.args('--project-name', dest='project_name',
                          help="OpenStack user tenant name",
                          default=os.environ.get('OS_TENANT_NAME'))
    @cli_application.args('--keystone-auth-url', dest='auth_url',
                          help="OpenStack Keystone auth URL",
                          default=os.environ.get('OS_AUTH_URL'))
    @cli_application.args('--auth-token', dest='auth_token',
                          help="Auth token, "
                               "for non-credentials auth",
                          default=os.environ.get('OS_TOKEN'))
    @cli_application.args('--project-id', dest='project_id',
                          help='OpenStack user tenant ID',
                          default=os.environ.get('OS_TENANT_ID'))
    @cli_application.args('--auth-region-name', dest='auth_region',
                          help='OpenStack Region name',
                          default=os.environ.get('OS_REGION_NAME'))
    @cli_application.args('--insecure', dest='insecure',
                          help="Enable SSL verification.",
                          default=False)
    def list(self, beedb_endpoint=None,
             username=None, password=None, project_name=None,
             auth_url=None, auth_token=None, project_id=None,
             auth_region=None, insecure=None):
        """
        Lists databases
        :param beedb_endpoint: BeeDB API endpoint
        :param username: OpenStack username
        :param password: OpenStack user password
        :param project_name: OpenStack user project name
        :param auth_url: OpenStack Keystone auth URL
        :param auth_token: OpenStack user auth token.
                           Required when no credentials were supplied.
        :param project_id: OpenStack user project ID
        :param auth_region: OpenStack auth region
        :param insecure: use insecure SSL connection, means server-side
                         does work over SSL/TLS but have
                         self-signed certificate
        :return:
        """

        bee = client.OpenStackBeeDBClient(
            beedb_endpoint,
            username,
            password,
            project_name,
            auth_url,
            auth_token,
            project_id,
            auth_region,
            enable_logging=False,
            insecure=insecure
        )
        print_utils.print_list(bee.databases.list(),
                               ['name', 'status',
                                'tenant', 'database_type',
                                'url', 'created_at',
                                'updated_at'])

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--database', dest='database',
                          help='Database identifier')
    @cli_application.args('--username', dest='username',
                          help="OpenStack username",
                          default=os.environ.get('OS_USERNAME'))
    @cli_application.args('--password', dest='password',
                          help="OpenStack user password",
                          default=os.environ.get('OS_PASSWORD'))
    @cli_application.args('--project-name', dest='project_name',
                          help="OpenStack user tenant name",
                          default=os.environ.get('OS_TENANT_NAME'))
    @cli_application.args('--keystone-auth-url', dest='auth_url',
                          help="OpenStack Keystone auth URL",
                          default=os.environ.get('OS_AUTH_URL'))
    @cli_application.args('--auth-token', dest='auth_token',
                          help="Auth token, "
                               "for non-credentials auth",
                          default=os.environ.get('OS_TOKEN'))
    @cli_application.args('--project-id', dest='project_id',
                          help='OpenStack user tenant ID',
                          default=os.environ.get('OS_TENANT_ID'))
    @cli_application.args('--auth-region-name', dest='auth_region',
                          help='OpenStack Region name',
                          default=os.environ.get('OS_REGION_NAME'))
    @cli_application.args('--insecure', dest='insecure',
                          help="Enable SSL verification.",
                          default=False)
    def get(self, beedb_endpoint=None,
            username=None, password=None, project_name=None,
            auth_url=None, auth_token=None, project_id=None,
            auth_region=None, insecure=None,
            database=None):
        """
        Shows database description
        :param beedb_endpoint: BeeDB API endpoint
        :param username: OpenStack username
        :param password: OpenStack user password
        :param project_name: OpenStack user project name
        :param auth_url: OpenStack Keystone auth URL
        :param auth_token: OpenStack user auth token.
                           Required when no credentials were supplied.
        :param project_id: OpenStack user project ID
        :param auth_region: OpenStack auth region
        :param database: Given database name
        :param insecure: use insecure SSL connection, means server-side
                         does work over SSL/TLS but have
                         self-signed certificate
        :return:
        """
        bee = client.OpenStackBeeDBClient(
            beedb_endpoint,
            username,
            password,
            project_name,
            auth_url,
            auth_token,
            project_id,
            auth_region,
            enable_logging=False,
            insecure=insecure
        )
        print_utils.print_dict(bee.databases.get(database).json_object)

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--database', dest='database',
                          help='Database identifier')
    @cli_application.args('--description', dest='description',
                          help="Database description.")
    @cli_application.args('--username', dest='username',
                          help="OpenStack username",
                          default=os.environ.get('OS_USERNAME'))
    @cli_application.args('--password', dest='password',
                          help="OpenStack user password",
                          default=os.environ.get('OS_PASSWORD'))
    @cli_application.args('--project-name', dest='project_name',
                          help="OpenStack user tenant name",
                          default=os.environ.get('OS_TENANT_NAME'))
    @cli_application.args('--keystone-auth-url', dest='auth_url',
                          help="OpenStack Keystone auth URL",
                          default=os.environ.get('OS_AUTH_URL'))
    @cli_application.args('--auth-token', dest='auth_token',
                          help="Auth token, "
                               "for non-credentials auth",
                          default=os.environ.get('OS_TOKEN'))
    @cli_application.args('--project-id', dest='project_id',
                          help='OpenStack user tenant ID',
                          default=os.environ.get('OS_TENANT_ID'))
    @cli_application.args('--auth-region-name', dest='auth_region',
                          help='OpenStack Region name',
                          default=os.environ.get('OS_REGION_NAME'))
    @cli_application.args('--insecure', dest='insecure',
                          help="Enable SSL verification.",
                          default=False)
    @cli_application.args('--role-name', dest='role',
                          help="Role for database assigning.")
    def create(self, beedb_endpoint=None,
               username=None, password=None, project_name=None,
               auth_url=None, auth_token=None, project_id=None,
               auth_region=None, insecure=None,
               database=None, description=None,
               role=None):
        """
        Creates a database by given name and description
        :param beedb_endpoint: BeeDB API endpoint
        :param username: OpenStack username
        :param password: OpenStack user password
        :param project_name: OpenStack user project name
        :param auth_url: OpenStack Keystone auth URL
        :param auth_token: OpenStack user auth token.
                           Required when no credentials were supplied.
        :param project_id: OpenStack user project ID
        :param auth_region: OpenStack auth region
        :param database: Given database name
        :param description: Given database description
        :param insecure: use insecure SSL connection, means server-side
                         does work over SSL/TLS but have
                         self-signed certificate
        :param role: role to assign to database
        :return:
        """
        bee = client.OpenStackBeeDBClient(
            beedb_endpoint,
            username,
            password,
            project_name,
            auth_url,
            auth_token,
            project_id,
            auth_region,
            enable_logging=False,
            insecure=insecure
        )
        print_utils.print_dict(bee.databases.create(
            database,
            role,
            description=description).json_object)

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--database', dest='database',
                          help='Database identifier')
    @cli_application.args('--username', dest='username',
                          help="OpenStack username",
                          default=os.environ.get('OS_USERNAME'))
    @cli_application.args('--password', dest='password',
                          help="OpenStack user password",
                          default=os.environ.get('OS_PASSWORD'))
    @cli_application.args('--project-name', dest='project_name',
                          help="OpenStack user tenant name",
                          default=os.environ.get('OS_TENANT_NAME'))
    @cli_application.args('--keystone-auth-url', dest='auth_url',
                          help="OpenStack Keystone auth URL",
                          default=os.environ.get('OS_AUTH_URL'))
    @cli_application.args('--auth-token', dest='auth_token',
                          help="Auth token, "
                               "for non-credentials auth",
                          default=os.environ.get('OS_TOKEN'))
    @cli_application.args('--project-id', dest='project_id',
                          help='OpenStack user tenant ID',
                          default=os.environ.get('OS_TENANT_ID'))
    @cli_application.args('--auth-region-name', dest='auth_region',
                          help='OpenStack Region name',
                          default=os.environ.get('OS_REGION_NAME'))
    @cli_application.args('--insecure', dest='insecure',
                          help="Enable SSL verification.",
                          default=False)
    def delete(self, beedb_endpoint=None,
               username=None, password=None, project_name=None,
               auth_url=None, auth_token=None, project_id=None,
               auth_region=None, insecure=None,
               database=None):
        """
        Deletes database
        :param beedb_endpoint: BeeDB API endpoint
        :param username: OpenStack username
        :param password: OpenStack user password
        :param project_name: OpenStack user project name
        :param auth_url: OpenStack Keystone auth URL
        :param auth_token: OpenStack user auth token.
                           Required when no credentials were supplied.
        :param project_id: OpenStack user project ID
        :param auth_region: OpenStack auth region
        :param database: Given database name
        :param insecure: use insecure SSL connection, means server-side
                         does work over SSL/TLS but have
                         self-signed certificate
        :return:
        """
        bee = client.OpenStackBeeDBClient(
            beedb_endpoint,
            username,
            password,
            project_name,
            auth_url,
            auth_token,
            project_id,
            auth_region,
            enable_logging=False,
            insecure=insecure
        )
        bee.databases.delete(database)
        print("OK. Done.")


class Roles(object):

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--role-name', dest='role',
                          help='Role name')
    @cli_application.args('--description', dest='description',
                          help="Database description.")
    @cli_application.args('--username', dest='username',
                          help="OpenStack username",
                          default=os.environ.get('OS_USERNAME'))
    @cli_application.args('--password', dest='password',
                          help="OpenStack user password",
                          default=os.environ.get('OS_PASSWORD'))
    @cli_application.args('--project-name', dest='project_name',
                          help="OpenStack user tenant name",
                          default=os.environ.get('OS_TENANT_NAME'))
    @cli_application.args('--keystone-auth-url', dest='auth_url',
                          help="OpenStack Keystone auth URL",
                          default=os.environ.get('OS_AUTH_URL'))
    @cli_application.args('--auth-token', dest='auth_token',
                          help="Auth token, "
                               "for non-credentials auth",
                          default=os.environ.get('OS_TOKEN'))
    @cli_application.args('--project-id', dest='project_id',
                          help='OpenStack user tenant ID',
                          default=os.environ.get('OS_TENANT_ID'))
    @cli_application.args('--auth-region-name', dest='auth_region',
                          help='OpenStack Region name',
                          default=os.environ.get('OS_REGION_NAME'))
    @cli_application.args('--insecure', dest='insecure',
                          help="Enable SSL verification.",
                          default=False)
    @cli_application.args('--role-password', dest='role_password',
                          help="Role for database assigning.")
    @cli_application.args('--database-type', dest='database_type',
                          help="Role for database assigning.")
    def create(self, beedb_endpoint=None,
               username=None, password=None, project_name=None,
               auth_url=None, auth_token=None, project_id=None,
               auth_region=None, insecure=None,
               role=None, description=None,
               role_password=None, database_type=None):
        """
        Creates a database by given name and description
        :param beedb_endpoint: BeeDB API endpoint
        :param username: OpenStack username
        :param password: OpenStack user password
        :param project_name: OpenStack user project name
        :param auth_url: OpenStack Keystone auth URL
        :param auth_token: OpenStack user auth token.
                           Required when no credentials were supplied.
        :param project_id: OpenStack user project ID
        :param auth_region: OpenStack auth region
        :param role_name: Given role name
        :param role_password: Given role password
        :param description: Given database description
        :param insecure: use insecure SSL connection, means server-side
                         does work over SSL/TLS but have
                         self-signed certificate
        :return:
        """
        bee = client.OpenStackBeeDBClient(
            beedb_endpoint,
            username,
            password,
            project_name,
            auth_url,
            auth_token,
            project_id,
            auth_region,
            enable_logging=False,
            insecure=insecure
        )
        print_utils.print_dict(bee.roles.create(
            role,
            role_password,
            database_type,
            description=description).json_object)

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--username', dest='username',
                          help="OpenStack username",
                          default=os.environ.get('OS_USERNAME'))
    @cli_application.args('--password', dest='password',
                          help="OpenStack user password",
                          default=os.environ.get('OS_PASSWORD'))
    @cli_application.args('--project-name', dest='project_name',
                          help="OpenStack user tenant name",
                          default=os.environ.get('OS_TENANT_NAME'))
    @cli_application.args('--keystone-auth-url', dest='auth_url',
                          help="OpenStack Keystone auth URL",
                          default=os.environ.get('OS_AUTH_URL'))
    @cli_application.args('--auth-token', dest='auth_token',
                          help="Auth token, "
                               "for non-credentials auth",
                          default=os.environ.get('OS_TOKEN'))
    @cli_application.args('--project-id', dest='project_id',
                          help='OpenStack user tenant ID',
                          default=os.environ.get('OS_TENANT_ID'))
    @cli_application.args('--auth-region-name', dest='auth_region',
                          help='OpenStack Region name',
                          default=os.environ.get('OS_REGION_NAME'))
    @cli_application.args('--insecure', dest='insecure',
                          help="Enable SSL verification.",
                          default=False)
    def list(self, beedb_endpoint=None,
             username=None, password=None, project_name=None,
             auth_url=None, auth_token=None, project_id=None,
             auth_region=None, insecure=None):
        """
        Lists databases
        :param beedb_endpoint: BeeDB API endpoint
        :param username: OpenStack username
        :param password: OpenStack user password
        :param project_name: OpenStack user project name
        :param auth_url: OpenStack Keystone auth URL
        :param auth_token: OpenStack user auth token.
                           Required when no credentials were supplied.
        :param project_id: OpenStack user project ID
        :param auth_region: OpenStack auth region
        :param insecure: use insecure SSL connection, means server-side
                         does work over SSL/TLS but have
                         self-signed certificate
        :return:
        """

        bee = client.OpenStackBeeDBClient(
            beedb_endpoint,
            username,
            password,
            project_name,
            auth_url,
            auth_token,
            project_id,
            auth_region,
            enable_logging=False,
            insecure=insecure
        )
        print_utils.print_list(bee.roles.list(),
                               ['name', 'status',
                                'tenant', 'database_type',
                                'password', 'created_at',
                                'updated_at'])

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--role-name', dest='role',
                          help='Role name')
    @cli_application.args('--username', dest='username',
                          help="OpenStack username",
                          default=os.environ.get('OS_USERNAME'))
    @cli_application.args('--password', dest='password',
                          help="OpenStack user password",
                          default=os.environ.get('OS_PASSWORD'))
    @cli_application.args('--project-name', dest='project_name',
                          help="OpenStack user tenant name",
                          default=os.environ.get('OS_TENANT_NAME'))
    @cli_application.args('--keystone-auth-url', dest='auth_url',
                          help="OpenStack Keystone auth URL",
                          default=os.environ.get('OS_AUTH_URL'))
    @cli_application.args('--auth-token', dest='auth_token',
                          help="Auth token, "
                               "for non-credentials auth",
                          default=os.environ.get('OS_TOKEN'))
    @cli_application.args('--project-id', dest='project_id',
                          help='OpenStack user tenant ID',
                          default=os.environ.get('OS_TENANT_ID'))
    @cli_application.args('--auth-region-name', dest='auth_region',
                          help='OpenStack Region name',
                          default=os.environ.get('OS_REGION_NAME'))
    @cli_application.args('--insecure', dest='insecure',
                          help="Enable SSL verification.",
                          default=False)
    def get(self, beedb_endpoint=None,
            username=None, password=None, project_name=None,
            auth_url=None, auth_token=None, project_id=None,
            auth_region=None, insecure=None,
            role=None):
        """
        Shows database description
        :param beedb_endpoint: BeeDB API endpoint
        :param username: OpenStack username
        :param password: OpenStack user password
        :param project_name: OpenStack user project name
        :param auth_url: OpenStack Keystone auth URL
        :param auth_token: OpenStack user auth token.
                           Required when no credentials were supplied.
        :param project_id: OpenStack user project ID
        :param auth_region: OpenStack auth region
        :param database: Given database name
        :param insecure: use insecure SSL connection, means server-side
                         does work over SSL/TLS but have
                         self-signed certificate
        :return:
        """
        bee = client.OpenStackBeeDBClient(
            beedb_endpoint,
            username,
            password,
            project_name,
            auth_url,
            auth_token,
            project_id,
            auth_region,
            enable_logging=False,
            insecure=insecure
        )
        print_utils.print_dict(bee.roles.get(role).json_object)

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--role-name', dest='role',
                          help='Role name')
    @cli_application.args('--username', dest='username',
                          help="OpenStack username",
                          default=os.environ.get('OS_USERNAME'))
    @cli_application.args('--password', dest='password',
                          help="OpenStack user password",
                          default=os.environ.get('OS_PASSWORD'))
    @cli_application.args('--project-name', dest='project_name',
                          help="OpenStack user tenant name",
                          default=os.environ.get('OS_TENANT_NAME'))
    @cli_application.args('--keystone-auth-url', dest='auth_url',
                          help="OpenStack Keystone auth URL",
                          default=os.environ.get('OS_AUTH_URL'))
    @cli_application.args('--auth-token', dest='auth_token',
                          help="Auth token, "
                               "for non-credentials auth",
                          default=os.environ.get('OS_TOKEN'))
    @cli_application.args('--project-id', dest='project_id',
                          help='OpenStack user tenant ID',
                          default=os.environ.get('OS_TENANT_ID'))
    @cli_application.args('--auth-region-name', dest='auth_region',
                          help='OpenStack Region name',
                          default=os.environ.get('OS_REGION_NAME'))
    @cli_application.args('--insecure', dest='insecure',
                          help="Enable SSL verification.",
                          default=False)
    def delete(self, beedb_endpoint=None,
               username=None, password=None, project_name=None,
               auth_url=None, auth_token=None, project_id=None,
               auth_region=None, insecure=None,
               role=None):
        """
        Deletes database
        :param beedb_endpoint: BeeDB API endpoint
        :param username: OpenStack username
        :param password: OpenStack user password
        :param project_name: OpenStack user project name
        :param auth_url: OpenStack Keystone auth URL
        :param auth_token: OpenStack user auth token.
                           Required when no credentials were supplied.
        :param project_id: OpenStack user project ID
        :param auth_region: OpenStack auth region
        :param database: Given database name
        :param insecure: use insecure SSL connection, means server-side
                         does work over SSL/TLS but have
                         self-signed certificate
        :return:
        """
        bee = client.OpenStackBeeDBClient(
            beedb_endpoint,
            username,
            password,
            project_name,
            auth_url,
            auth_token,
            project_id,
            auth_region,
            enable_logging=False,
            insecure=insecure
        )
        bee.roles.delete(role)
        print("OK. Done.")


class DatabaseTypes(object):

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--username', dest='username',
                          help="OpenStack username",
                          default=os.environ.get('OS_USERNAME'))
    @cli_application.args('--password', dest='password',
                          help="OpenStack user password",
                          default=os.environ.get('OS_PASSWORD'))
    @cli_application.args('--project-name', dest='project_name',
                          help="OpenStack user tenant name",
                          default=os.environ.get('OS_TENANT_NAME'))
    @cli_application.args('--keystone-auth-url', dest='auth_url',
                          help="OpenStack Keystone auth URL",
                          default=os.environ.get('OS_AUTH_URL'))
    @cli_application.args('--auth-token', dest='auth_token',
                          help="Auth token, "
                               "for non-credentials auth",
                          default=os.environ.get('OS_TOKEN'))
    @cli_application.args('--project-id', dest='project_id',
                          help='OpenStack user tenant ID',
                          default=os.environ.get('OS_TENANT_ID'))
    @cli_application.args('--auth-region-name', dest='auth_region',
                          help='OpenStack Region name',
                          default=os.environ.get('OS_REGION_NAME'))
    @cli_application.args('--insecure', dest='insecure',
                          help="Enable SSL verification.",
                          default=False)
    def list(self, beedb_endpoint=None,
             username=None, password=None, project_name=None,
             auth_url=None, auth_token=None, project_id=None,
             auth_region=None, insecure=None):
        bee = client.OpenStackBeeDBClient(
            beedb_endpoint,
            username,
            password,
            project_name,
            auth_url,
            auth_token,
            project_id,
            auth_region,
            enable_logging=False,
            insecure=insecure
        )
        print_utils.print_list(bee.database_types.list(),
                               ['name', 'description',
                                'created_at', 'updated_at'])

CATS = {
    'databases': Databases,
    'roles': Roles,
    'database-types': DatabaseTypes,
}

category_opt = cfg.SubCommandOpt(
    'category',
    title='Command categories',
    help='Available categories',
    handler=cli_application.add_command_parsers(CATS)
)


def main():
    """Parse options and call the appropriate class/method."""
    cli_application.main(cfg.CONF, category_opt)

if __name__ == "__main__":
    main()
