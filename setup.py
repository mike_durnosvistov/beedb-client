import beedb_client
import os
import setuptools


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

with open('requirements.txt') as f:
    required = f.read().splitlines()

setuptools.setup(
    name='beedb-client',
    version=beedb_client.VERSION,
    description='BeeDB Pure DBaaS for cloud environments',
    long_description=read('README.rst'),
    url='https://beedb.io',
    author='Denis Makogon',
    author_email='lildee1991@gmail.com',
    packages=setuptools.find_packages(),
    install_requires=required,
    license='License :: OSI Approved :: Apache Software License',
    classifiers=[
        'License :: OSI Approved :: Apache Software License',
        'Intended Audience :: Information Technology',
        'Intended Audience :: System Administrators',
        'Intended Audience :: Developers',
        'Environment :: No Input/Output (Daemon)',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Topic :: Software Development :: Libraries'
        ' :: Python Modules',
        'Topic :: System :: Distributed Computing',
        'Operating System :: Microsoft :: Windows',
        'Operating System :: POSIX',
        'Operating System :: Unix',
        'Operating System :: MacOS',
    ],
    keywords='BeeDB OpenStack AmazonWS, vCloudAir',
    platforms=['Windows', 'Linux', 'Solaris', 'Mac OS-X', 'Unix'],
    test_suite='tests',
    tests_require=[],
    zip_safe=True,
    entry_points={
        'console_scripts': [
            'beedb-aws-client = '
            'beedb_client.v1.aws.shell:main',
            'beedb-openstack-client = '
            'beedb_client.v1.openstack.shell:main',
            'beedb-vcloud-air-client = '
            'beedb_client.v1.vcloudair.shell:main'
        ]
    },
)
