===================================
BeeDB client. Pure DBaaS for clouds
===================================

-----------
Description
-----------

API client for BeeDB service

----------------------
Supported environments
----------------------

In terms of more flexible integration BeeDB client and server side does support next environments::

    OpenStack
    vCloud Air
    AWS

------------
Installation
------------

In order to install BeeDB client please follow this commands::

    git clone https://github.com/ProjectInvader/beedb-client.git
    cd beedb-client
    python setup.py install

Once it was done, you will have an ability to work with BeeDB using next CLI tools::

    beedb-openstack-client
    beedb-vcloudair-client
    beedb-aws-client

-----
Usage
-----

To simplify CLI tool usage, BeeDB client was designed to work with OS environment variables.
Please take a look at samples directory.
In order to source all necessary parameters for authorization you need to do next::

    source openstack.rc
    source aws.rc
    source vcloud-air-ondemand.rc
    source vcloud-air-subscription.rc

Once it was done, you can consume next API::

    beedb-openstack-client roles list

    beedb-openstack-client roles create --role-name <role>
        --role-password <role> \
        --description <role>

    beedb-openstack-client roles get --role-name <role>

    beedb-openstack-client roles list

    beedb-openstack-client roles delete --role-name

    beedb-openstack-client databases list

    beedb-openstack-client databases get --database <database>

    beedb-openstack-client databases create --database <database> \
        --description <description> \
        --role <role> \

    beedb-openstack-client databases delete --database <database>

--------------------
AWS CLI client usage
--------------------

This sub-chapter will add more information regarding CLI tool usage.
CLI tool expects next additional attributes::

    --beedb-endpoint        - BeeDB endpoint
    --aws-sts-endpoint-url  - AWS STS endpoint URL
    --access-key-id         - AWS user access_key_id
    --secret-access-key     - AWS user secret_access_key
    --region                - AWS region

    --database          - database name to create or look for
    --role-name         - database assigned role name
    --role-password     - database assigned role passowrd
    --description       - database or role description/purpose

--------------------------
OpenStack CLI client usage
--------------------------

This sub-chapter will add more information regarding CLI tool usage.
CLI tool expects next additional attributes::

    --beedb-endpoint    - BeeDB endpoint
    --username          - OpenStack username
    --password          - OpenStack user password
    --project-name      - OpenStack Project name
    --keystone-auth-url - OpenStack Identity service URL
    --auth-token        - Authorization token (alternative to username/password)
    --project-id        - OpenStack Project ID
    --auth-region-name  - OpenStack Region name
    --insecure          - flag to check secure SSL/TLS connection

    --database          - database name to create or look for
    --role-name         - database assigned role name
    --role-password     - database assigned role passowrd
    --description       - database or role description/purpose

----------------------
vCloudAir client usage
----------------------

This sub-chapter will add more information regarding CLI tool usage.
CLI tool expects next additional attributes::

    --beedb-endpoint              - BeeDB endpoint
    --username                    - vCloud Air username
    --password                    - vCloud Air user password
    --service-type                - vCloud Air service type (ondemand/subscription)
    --vcloud-host                 - vCloud Air host
    --service-version             - vCloud Air service type version
    --vcloud-instance             - vCloud Air user instance to log in to
    --vcloud-organization         - vCloud Air user organization to log in to
    --vcloud-organization-service - vCloud Air organiztion service to log in to

    --database          - database name to create or look for
    --role-name         - database assigned role name
    --role-password     - database assigned role passowrd
    --description       - database or role description/purpose

--------------
CLI usage note
--------------

As you can see in previous sections, you can use operating system
environment-based variables or you can use positional arguments for each CLI command.

--------------------
RESTful client usage
--------------------

Below you can see example of how to connect and consume BeeDB within its API client::

        from beedb_client.v1.openstack import client

        bee = client.OpenStackBeeDBClient(
            beedb_endpoint,
            username,
            password,
            project_name,
            auth_url,
            auth_token,
            project_id,
            auth_region,
            enable_logging=False,
            insecure=insecure
        )
        db_types = bee.database_types.list()
        roles = bee.roles.list()
        dbs = bee.databases.list()

------------------------------
Database provisioning workflow
------------------------------

In order to get accessible database you need to::

    List and pick database type
    Create role
    Create database assigned to role

Here's an example how it can be done within CLI::

    beedb-openstack-client database-types list

    +-------------------------+-------------+----------------------------+----------------------------+
    | Name                    | Description | Created At                 | Updated At                 |
    +-------------------------+-------------+----------------------------+----------------------------+
    | postgresql-cluster-east | None        | 2015-10-21 10:36:53.309074 | 2015-10-21 10:36:53.309081 |
    +-------------------------+-------------+----------------------------+----------------------------+


    beedb-openstack-client roles create --role-name role \
        --role-password password \
        --description "General purpose role"
        --database-type postgresql-cluster-east

    +---------------+----------------------------------+
    | Property      | Value                            |
    +---------------+----------------------------------+
    | created_at    | 2015-10-05 10:01:46.886941       |
    | database_type | postgresql-cluster-east          |
    | description   | General purpose role             |
    | name          | role                             |
    | password      | role                             |
    | status        | AVAILABLE                        |
    | tenant        | 2569eafc8b404633976e24e8b5f909f5 |
    | updated_at    | 2015-10-05 10:01:46.886951       |
    +---------------+----------------------------------+

    beedb-openstack-client roles list

    +------+-----------+----------------------------------+-------------------------+----------+----------------------------+----------------------------+
    | Name | Status    | Tenant                           | Database Type           | Password | Created At                 | Updated At                 |
    +------+-----------+----------------------------------+-------------------------+----------+----------------------------+----------------------------+
    | role | AVAILABLE | 2569eafc8b404633976e24e8b5f909f5 | postgresql-cluster-east | role     | 2015-10-05 10:01:46.886941 | 2015-10-05 10:01:46.886951 |
    +------+-----------+----------------------------------+-------------------------+----------+----------------------------+----------------------------+

    beedb-openstack-client roles get --role-name role

    +---------------+----------------------------------+
    | Property      | Value                            |
    +---------------+----------------------------------+
    | created_at    | 2015-10-21 11:17:05.391638       |
    | database_type | postgresql-cluster-east          |
    | description   | role                             |
    | name          | role                             |
    | password      | role                             |
    | status        | AVAILABLE                        |
    | tenant        | 92553da2235a4510aa5954bcf3490803 |
    | updated_at    | 2015-10-21 11:17:05.391720       |
    +---------------+----------------------------------+

    beedb-openstack-client databases create --role-name role \
        --database database \
        --description "General purpose database"

    +---------------+-------------------------------------------------------+
    | Property      | Value                                                 |
    +---------------+-------------------------------------------------------+
    | created_at    | 2015-10-05 10:04:16.800184                            |
    | database_type | PostgreSQL                                            |
    | id            | 3c32f0cb-f829-46bb-8d03-1e1adc1bc844                  |
    | name          | database                                              |
    | status        | AVAILABLE                                             |
    | tenant        | 2569eafc8b404633976e24e8b5f909f5                      |
    | updated_at    | 2015-10-05 10:04:16.814206                            |
    | url           | postgresql://role:password@10.23.12.221:5432/database |
    +---------------+-------------------------------------------------------+



As you can see, in database provisioning you can see connection URL to the database
that is can be consumed by other services as connection attributes for persistent layer.

------------
Contribution
------------

In order to file bugs use BitBucket ticketing system.
Same thing for features.
